#!/usr/bin/env python3
import requests
import json

#Please put your name, as a string
YOUR_NAME=""
PATH = ""

def getPayload(wip, name):
    dPayload = {"channel": "#academy_api_testing", "username": "IP Checker"}
    dPayload['text'] = str(name+"'s IP has changed to "+wip)
    jPayload = json.loads(json.dumps(dPayload))
    return jPayload

def send2Slack(wip, name):
    SLACK_URL = "https://hooks.slack.com/services/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"
    rSlack = requests.post(SLACK_URL, json=getPayload(wip, name))
    if rSlack.status_code != 200:
        print("requests.post error")
        sys.exit(2)

filestring = PATH+'/ipFile'
fh = open(filestring, 'r')
fip = fh.readline().strip()
fh.close()

IP_URL = 'https://api.ipify.org/?format=json'
r = requests.get(IP_URL)
if r.status_code != 200:
    print("requests.get error")
    sys.exit(1)
rjson = json.loads(r.content)

wip = rjson['ip']

if wip != fip:
    fh = open(filestring, 'w')
    fh.write(wip)
    fh.close()
    send2Slack(wip, YOUR_NAME)
