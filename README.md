# Automated IP Checker

### You'll need the requests module for this to work

1. Put your name and the path to this folder at the top of ipChecker.py
2. Put your current IP on line 1 of ipFile
3. Add to the bottom of your ~/.zshrc file: python3 /<full_path_here>/ipChecker.py
